#!/bin/bash

gisturl="https://gist.githubusercontent.com/thetre97/a7c9cd2c0b5873986fb8e7557de95971/raw"

curl -k -o ../extensions.json "${gisturl}/extensions.json"
curl -k -o ../settings.json "${gisturl}/settings.json"
